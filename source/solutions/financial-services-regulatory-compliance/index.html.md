---
layout: markdown_page
title: "Financial Services Regulatory Compliance"
---

## Introduction

GitLab is used extensively to achieve regulatory compliance in the financial services industry. Many of the world's largest financial institutions are GitLab customers.
This page details the relevant rules, the principles needed to achieve them, and the features in GitLab that make that possible.

## Regulators and regulations

Examples of regulators include the following
1. America: [FEB](https://www.federalreserve.gov/) in the US, also see the [FFIEC IT Handbook](https://ithandbook.ffiec.gov/)
1. Europe: [FCA](https://www.fca.org.uk/) and [PRA](https://www.bankofengland.co.uk/prudential-regulation) in the UK, [FINMA](https://www.finma.ch/en/) in Switzerland
1. Asia: [MAS](http://www.mas.gov.sg/) in Singapore and [HKMA](http://www.hkma.gov.hk/eng/index.shtml)

Examples of relevant regulations include the following
1. GLBA Safeguards rule requires that financial institutions must protect the consumer information they collect and hold service providers to same standards.
1. Dodd-Frank’s purpose is to promote the financial stability of the United States by improving accountability and transparency in the financial system. It sets the baseline for what is “reasonable and appropriate” security around consumer financial data. You must be ready to prove your security controls and document them.
1. Sarbanes Oxley (SOX) exists to protect investors by improving the accuracy and reliability of corporate disclosures made pursuant to the securities laws, and for other purposes.  Advice for achieving this is augmented by other frameworks such as COBIT14 and the CIS Critical Security Controls.
1. PCI DSS is intended to maintain payment security and is required for all entities that store, process or transmit cardholder data. It requires companies using credit cards to protect cardholder data,  manage vulnerabilities, provide strong access controls, monitor and test, and maintain policy.

Specific controls common amongst these regulations are outlined below, along with features of GitLab that aid in their compliance.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-xldj{border-color:inherit;text-align:left}
</style>
<table class="tg" style="undefined;table-layout: fixed; width: 100%">
<colgroup>
<col style="width: 90px">
<col style="width: 200px">
<col style="width: 200px">
<col style="width: 200px">
</colgroup>
  <tr>
    <th class="tg-xldj">Control</th>
    <th class="tg-xldj">Rule</th>
    <th class="tg-xldj">Principles</th>
    <th class="tg-xldj">How GitLab helps</th>
  </tr>
  <tr>
    <td class="tg-xldj">Segregation of Incompatible Duties (SODs)</td>
    <td class="tg-xldj">To protect a system from unauthorized changes and fraud, organizations must establish organization-defined duties and roles, document separation of duties of these indivudlas and roles, and define assocaited system access authorizations to support these separation of duties.</td>
    <td class="tg-xldj">* You never merge your own code.<br>* All code needs to be peer reviewed.<br>* Only authorized people can approve the code.<br>* You need a log of who approved it.</td>
    <td class="tg-xldj"><a href="https://docs.gitlab.com/ee/user/permissions.html">1. Defined Project Permissions</a><br><a href="https://docs.gitlab.com/ee/user/project/protected_branches.html">2. Protected branches</a><br>
<a href="https://docs.gitlab.com/ee/ci/environments/protected_environments.html">3. Protected environments</a><br>
<a href="https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html">4. Merge request approvals</a><br>
<a href="https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches">5. Unprotect permission</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ce/issues/44041">6. Future: Approval jobs in CI pipelines</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/7176">7. Future: Two-person access controls</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/4418">8. Future: Require merge request approval by code owners</a><br></td>
  </tr>
  <tr>
  <td class="tg-xldj">Identity and Access Approval Controls to Ensure Proper SODs</td>
    <td class="tg-xldj">Separation of duties addresses the potential for abuse of authorized privileges and helps to reduce the risk of malevolent activity without collusion. </td>
    <td class="tg-xldj">* Divide mission functions and information system support functions among different individuls/roles.<br>* Segregate configuration management, programming, system management, quality assurance functions.<br>* Only authorized people can approve the code.<br>* Only authorized roles can merge code.</td>
    <td class="tg-xldj">1. Role-Based Access Controls (RBAC) within protected branches and environments prevent unauthorized users from deploying to labeled environments.<br>
<a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/1979">2. Future: Enable Multiple rules for merge request approvals Code</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/3845">3. Future: Prevent merge request and commit authors from self-approving</a><br><a href="https://gitlab.com/gitlab-org/gitlab-ce/issues/53906">4. Future: Identity API</a><br>
</td>
  </tr>
  <tr>
   <td class="tg-xldj">Configuration Management</td>
    <td class="tg-xldj">NIST's Configuration Management control as outlined in NIST 800-53, Rev. 4: CM-2 establishes that baseline configurations are documented, formally reviewed and agreed-upon. As baseline configurations serve as a basis for future builds, releases, and/or changes to information systems, it is critical for organizations to have the ability to control changes and evidence the integrity of the deployment process.</td>
    <td class="tg-xldj">* Baseline configurations are stored and tracked.<br>* Automated configuration management is employed to remove manual, error-prone processes.<br>* Changes to configurations are approved.<br>* Logs of changes are maintained.</td>
    <td class="tg-xldj"><a href="https://docs.gitlab.com/ee/ci/yaml/README.html">1. CI/CD configurations</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ce/issues/44041">2. Future: Approval jobs in CI pipelines</a><br>
</td>
  </tr>
  <tr>
  <td class="tg-xldj">Configuration Change Control</td>
    <td class="tg-xldj">Configuration Change Control is outlined in NIST 800-53, Rev. 4: CM-3: the organization implements approved configuration controlled changes to the information system and retains a record of the configuration controlled-changes - and changes to deployment configurations.</td>
    <td class="tg-xldj">* All changes to baseline configurations are stored and tracked.<br>* Changes to configurations of protected branch and environment configurations are stored and tracked.<br>* Logs of changes are maintained.</td>
    <td class="tg-xldj"><a href="https://about.gitlab.com/handbook/marketing/product-marketing/demo/cicd-deep/">1. CI/CD pipeline configuration management</a><br><a href="https://docs.gitlab.com/ee/administration/audit_events.html">2. Audit Events</a><br>CI/CD pipeline configurations are tracked in source controls in the same manner as your source code; any changes to these deployment configurations are logged as part of your source code control, thus ensuring that GitLab's release orchestration processes directly support change control best practices.
</td>
</tr>
  <tr>
  <td class="tg-xldj">Access Restrictions for Changes to Configurations and Pipelines</td>
    <td class="tg-xldj">ISO 27002 9 (Access Controls) and NIST 800-53, Rev. 4: CM-5 and AC-3 (Logical Access Enforcement) dictate that organization defines, documents, approves, and enforces logical access restrictions associated with changes to the information system.  Any changes to the software can potentially have significant effects on the overall security of the systems. Therefore, organizations permit only qualified and authorized individuals to access information systems for purposes of initiating changes, including upgrades and modifications.</td>
    <td class="tg-xldj">* Controls exist to prevent initiation of pipelines without requisite approvals.<br>* Only authorized users can deploy to production.</td>
    <td class="tg-xldj"><a href="https://docs.gitlab.com/ee/user/project/protected_branches.html">1. Protected branches</a><br>
<a href="https://docs.gitlab.com/ee/ci/environments/protected_environments.html">2. Protected environments</a><br>
<a href="https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html">3. Merge request approvals</a><br>4. Access permissions to the CI .yaml file are the same as those set for your repositories.
</td>
  </tr>
  <tr>
    <td class="tg-xldj">Security</td>
    <td class="tg-xldj"> Both the NIST and the ISO security frameworks outline requirements related to developer security testing and evaluation. NIST 800-53, Rev. 4: SA -11 establishes that organization must require the developers of the information system, system component, or information service to implement a security assessment plan, produce evidence of execution of security testing, and correct flaws identified. <br> As a result, business application software needs to support the following: <br>
    * Evidence that data has not been modified. <br>
    * Role-based access and revocation of accounts.<br>  
    * Auditing and logging of events in systems that process sensitive data.<br>  
    * Log system changes in a way that those logs are resistant to tampering and accessible only to privileged users.<br>
    Note: Application Security Testing can help identify vulnerabilities that enable unauthorized access to data, logic, and reporting. </td>
    <td class="tg-xldj">* Scan applications regularly for vulnerabilities.<br>* Establish criteria for the prioritization of vulnerabilities and remediation activities.<br>* Pay special attention to internally or custom developed applications with dynamic and static analysis.<br>* Establish secure coding as a culture, and provide qualified training on secure coding.<br>* Establish and document a secure development life-cycle approach that fits your business and developers.<br>* Combine functional testing and security testing of applications: Assess for operational bugs and coding errors.</td>
    <td class="tg-xldj">
    <a href="https://docs.gitlab.com/ee/user/project/merge_requests/sast.html">1. SAST</a><br>
    <a href="https://docs.gitlab.com/ee/user/project/merge_requests/dast.html">2. DAST</a><br>
    <a href="https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html">3. Dependency Scanning</a><br>
    <a href="https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html">4. Container Scanning</a><br>
    <a href="https://about.gitlab.com/2018/07/22/gitlab-11-1-released/#security-dashboard-for-projects">5. Security Dashboard</a><br>
    <a
    href="https://about.gitlab.com/handbook/product/#security-paradigm">6. Security Paradigm</a><br><a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/8481">7. Future: Show Dependency Scanning results in Groups Security Dashboard</a><br>
    In addition to Application Security Testing to help you deliver secure apps, GitLab's own application has security to prevent unauthorized access to the application code as well as audit and logging capabilities of changes to the code.</td>
    </tr>
    <tr>
     <td class="tg-xldj">Operations Security via Protections on Branches and Environments</td>
    <td class="tg-xldj">Organizations need to evidence the integrity of their production environments and critical code branches. </td>
    <td class="tg-xldj">* Changes to configuration of protections for branches and environments are tracked and logged.<br>* Organizations must have access to readily available, human-readable audit trail of all actions taken to label and protect environments.<br>* Controls exist to prevent unauthorized parties from running deployment jobs labeled as protected.</td>
    <td class="tg-xldj">1. Granular user-based and role-based (RBAC) access controls for branches, environments and Kubernetes clusters supply operators the ability to determine who can deploy and to what environments.<br><a href="https://docs.gitlab.com/ee/user/project/protected_branches.html">2. Protected branches allow you to protect your production code pipeline.</a><br>
<a href="https://docs.gitlab.com/ee/ci/environments/protected_environments.html">3. Protected environments allow you to protect your environments by preventing deployments to them by certain individuals.</a><br>
</td>
  </tr>
  <tr>
    <td class="tg-xldj">Auditing</td>
    <td class="tg-xldj">Systems must have clear audit logs to trace changes to the data and also to the logic flow.</td>
    <td class="tg-xldj">* Auditability of the production application: Software systems must generate all of the necessary logging information to construct a clear audit trail that shows how a user or entity attempts to access and utilize resources.<br>
    * Auditability of the software itself to detect changes in logic flow: Whether urban legend or not, the example is relevant of the developer who pockets rounding errors to his own bank account<br>  
    * Logs must be resistant to tampering and accessible only to privileged users.</td>
    <td class="tg-xldj">1. One concept of a user across the lifecycle to ensure the right level of permissions and access<br>
    <a
    href="https://docs.gitlab.com/ee/administration/logs.html">2. Audit logs</a><br>
    <a
    href="https://docs.gitlab.com/ee/administration/audit_events.html">3. Audit events</a><br>
    <a
    href="https://docs.gitlab.com/omnibus/docker/README.html#where-is-the-data-stored">4. Container image retention</a><br>
    <a
    href="https://docs.gitlab.com/ee/administration/job_artifacts.html#storing-job-artifacts">5. Artifact retention</a><br>
    <a
    href="https://docs.gitlab.com/ee/development/testing_guide/end_to_end_tests.html#testing-code-in-merge-requests">6. Test result retention</a><br>
    <a
    href="https://gitlab.com/gitlab-org/gitlab-ee/issues/5297">7. Future: Disable squash of commits</a><br>
    8. Future: Prevent purge<br></td>
    
  </tr>
  <tr>
    <td class="tg-xldj">Change Management</td>
    <td class="tg-xldj">All changes must be tracked. Compliance with Sarbanes-Oxley mandates that publicly traded companies report  any material changes to the processes and systems that govern the flow of financial data so as to reduce the risk of material misstatement of financial statements.</td>
    <td class="tg-xldj">* Systems used to support change management procedures must retain records of changes made, of who reviewed and approved changes, and the sequence in which they were performed.<br>* Changes should be made in such a way that they can be rolled back to a previous version quickly and easily. Here are two examples as to why: Flash Crash and TSB Bank Disaster.<br>* Source control systems should prevent unauthorized changes using access control or, at least showing changes for a clear audit trail.</td>
    <td class="tg-xldj"><a href="https://docs.gitlab.com/ee/topics/autodevops/index.html#auto-deploy">1. Automated deploy</a><br><a href="https://docs.gitlab.com/ee/update/6.3-to-6.4.html#1-revert-the-code-to-the-previous-version">2. Revert button</a><br> <a href="https://docs.gitlab.com/ee/ci/review_apps/index.html#overview">3. Review apps</a> make it easy to visualize the changes in code review and ensure changes function in a legitimate manner.<br>4. Issues Boards that enable integration of project management tools into your change management procedures.<br>
    <a
    href="https://gitlab.com/groups/gitlab-org/-/epics/269">5. Future: End-to-end traceability for merges</a><br><a
    href="https://gitlab.com/gitlab-org/gitlab-ce/issues/51738">6. Future: Blackout periods to ensure production change freezes</a><br><a
    href="https://gitlab.com/gitlab-org/gitlab-ee/issues/8373">7. Future: Automated incident creation integration to support change management policies</a><br></td>
  </tr>
  <tr>
    <td class="tg-xldj">License Code Usage</td>
    <td class="tg-xldj">Third party and open source code use must comply with license constraints.</td>
    <td class="tg-xldj">* To comply with license constraints, you must track license expiration and usage. This is important to manage risk from legal costs for license agreement violations and risk to your reputation.</td>
    <td class="tg-xldj"><a href="https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html">1. License Management</a><br></td>
  </tr>
</table>


## Learn how GitLab can help solve your compliance challenge

[Contact sales](https://about.gitlab.com/sales)

## Reference articles of interest

1. [DevOps Survival](https://www.infoq.com/news/2016/07/devops-survival-finance)
2. [How the Federal Reserve Bank of New York Navigates the Supply Chain of Open Source](https://www.hpe.com/us/en/insights/articles/how-the-federal-reserve-bank-of-new-york-navigates-the-supply-chain-of-open-source-software-1710.html)
3. [Primer Ensuring Regulatory Compliance in Cloud Deployments](https://www.hpe.com/us/en/insights/articles/primer-ensuring-regulatory-compliance-in-cloud-deployments-1704.html)
4. [Understanding Security Regulations in the Financial Services Industry](https://www.sans.org/reading-room/whitepapers/analyst/understanding-security-regulations-financial-services-industry-37027)
5. [Regulatory Compliance Demystified](https://msdn.microsoft.com/en-us/library/aa480484.aspx#regcompliance_demystified_topic5)
6. [National Institure of Standards and Technology Security Controls](https://nvd.nist.gov/800-53/Rev4)
7. [Internal Organization for Standardization ISO/IEC 27002:2013 Information Technology - Code of Practice for information security controls](https://www.iso.org/standard/54533.html)