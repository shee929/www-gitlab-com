---
layout: markdown_page
title: "Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

| **Workflow** | [**How may we be of service?**](production/#workflow--how-we-work) | **GitLab.com Status** | [**`STATUS`**](https://status.gitlab.com/)
| **Issue Trackers** | [**Infrastructure**](https://gitlab.com/gitlab-com/infrastructure/issues/): [Milestones](https://gitlab.com/gitlab-com/infrastructure/milestones), [OnCall](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=oncall) | [**Production**](https://gitlab.com/gitlab-com/production/issues/): [Incidents](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=incident), [Changes](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=change), [Deltas](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=delta)  | [**Delivery**](https://gitlab.com/gitlab-org/release/framework)
| **Slack Channels** | [#sre-lounge](https://gitlab.slack.com/archives/sre-lounge), [#database](https://gitlab.slack.com/archives/database) | [#alerts](https://gitlab.slack.com/archives/alerts), [#production](https://gitlab.slack.com/archives/production) | [#g_delivery](https://gitlab.slack.com/archives/g_delivery)
| **Operations** | [Runbooks](https://gitlab.com/gitlab-com/runbooks) (please contribute!) | **On-call**: [Handover Document](https://docs.google.com/document/d/1IrTi06fUMgxqDCDRD4-e7SJNPvxhFML22jf-3pdz_TI), [Reports](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=oncall%20report) |

## Teams

An operational environment is a complex and interconnected mesh of components working in unison to deliver a set of
services. Rather than organize the team along siloed functional groups, our team is aligned with the environment's
**lifecycle**, taking into account the two variables that drive change into the environment: time and space. Events
and actions take place in the environment in a time scale (between essentially _now_ and _soon_) and their effect on
people resources is higher the closer said resources are to the environment.

### Structure

Our long-term objective is to become a world-class SRE organization. In order to reach that goal, we are adopting a
**focal** arrangement where the organizational formula is derived from the focus and purpose of the groups arranged
along the time and space variables, and each group contains the appropriate functional resources necessary to manage
the environment, which include systems and database specialties.

The [first iteration](https://about.gitlab.com/handbook/values/#iteration) in this model comprised two groups, **Site
Availability** and **Site Reliability**.

The **second iteration** adds a third group, one specializing on the biggest source of change in the environment, 
releases, whose purpose is to make [CI/CD at GitLab](/handbook/engineering/infrastructure/blueprint/ci-cd) a reality:
**Delivery**.

Thus, the three groups:

* [**Site Availability**](sae/), which operates on the _here_ and _now_ and is focused on uptime as its driving force.
* [**Site Reliability**](sre/), which operates on the _soon_ time horizon and is focused on efficiency, and of course, reliability.
* [**Delivery**](delivery/), which focuses on GitLab's delivery of software releases to GitLab.com and the public at large

![](img/GitLabInfraOrgStructureV3.png)

#### Rotation

Team members in Site Availability and Site Reliability rotate between both groups on a 6-9 month schedule to ensure that
all team members level on the skills necessary to be successful in our long-term vision. The rotation allows each and
every one of us to get a sense of the priority axes across both groups, which will eventually merge under a single SRE
umbrella.

#### Long-term Structure

As our processes and automation mature, the quality of our work will stabilize and be more predictable. We will become
adept at maintaining high levels of uptime across the board. **Site Availability** will then merge into
**Site Reliability**, at which point we will have several vertical **Site Reliability** teams that follow the sun.
GitLab.com is a global service, and as such, so must be Infrastructure.



