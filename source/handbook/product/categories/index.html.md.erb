---
layout: markdown_page
title: Product categories
extra_js:
    - listjs.min.js
    - categories.js
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Quick links

[Manage](#manage) | [Plan](#plan) | [Create](#create) | [Verify](#verify) | [Package](#package) | [Release](#release) | [Configure](#configure) | [Monitor](#monitor) | [Secure](#secure)

## Interfaces

We want intuitive interfaces both within the company and with the wider
community. This makes it more efficient for everyone to contribute or to get
a question answered. Therefore, the following interfaces are based on the
product categories defined on this page:

- [Home page](/)
- [Product page](/product/)
- [Product Features](/features/)
- [Pricing page](/pricing/)
- [DevOps Lifecycle](/stages-devops-lifecycle/)
- [DevOps Tools](/devops-tools/)
- [Product Vision](/direction/#vision)
- [Stage visions](/direction/#devops-stages)
- [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
- [Engineering](https://about.gitlab.com/handbook/engineering/) Engineering Manager/Developer/Designer titles, their expertise, and department, and team names.
- [Product manager](https://about.gitlab.com/handbook/product/) responsibilities which are detailed on this page
- [Our pitch deck](https://about.gitlab.com/handbook/marketing/product-marketing/#company-pitch-deck), the slides that we use to describe the company
- [Product marketing](https://about.gitlab.com/handbook/marketing/product-marketing/) specializations

## Hierarchy

The categories form a hierarchy:

1. **Departments**: Dev and Ops. Dev and Ops map to departments in our [organization structure](https://about.gitlab.com/company/team/structure/#table). At GitLab the Dev and Ops split is different then the infinity loop suggests because our CI/CD functionality is one codebase, so from verify on we consider it Ops so that the codebase falls under one department.
The stages that are the difference between the value stages and the team stages are part of the Dev department.
1. **Stages**: Stages start with the 7 **loop stages**, then add Manage and Secure to give the 9 (DevOps) **value stages**, and then add Distribution, Geo, Gitaly, Growth & Gitter to get the 13 **team stages**. Values stages are what we all talk about in our marketing. Each of the team stages has a dedicated engineering team, product manager. Within shared functions, like quality and product management, individuals are paired to one or more stages so that there are stable counterparts.
1. **Categories**: High-level capabilities that may be a standalone product at another company. e.g. Portfolio Management. There are a maximum of 8 high-level capabilities per stage to ensure we can display this on our website and pitch deck.
1. **Features**: Small, discrete functionalities. e.g. Issue weights. Some common features are listed within parentheses to facilitate finding responsible PMs by keyword. Features are maintained in [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml).

Every category listed on this page must have a link, determined by what exists
in the following hierarchy:

Marketing product page > docs page > epic > label query > issue

E.g if there's no marketing page, link to the docs. If there's no docs, link to
the Epic. Etc.

[Solutions](#solutions) can consist of multiple categories as defined on this
page, but there are also other ones, for example industry verticals. Solutions typically represent a customer challenge, how GitLab capabilities come together to meet that challenge, and business benefits of using our solution.

Capabilities can refer to stages, categories, features, but not solutions.

Adding more layers to the hierarchy would give it more fidelity but would hurt
usability in the following ways:

1. Harder to keep the [interfaces](#Interfaces) up to date.
1. Harder to automatically update things.
1. Harder to train and test people.
1. Harder to display more levels.
1. Harder to reason, falsify, and talk about it.
1. Harder to define what level something should be in.
1. Harder to keep this page up to date.

## Changes

The impact of changes to stages is felt [across the company](/company/team/structure/#stage-groups).
Merge requests with changes to stages and significant changes to categories need
to be approved by:

1. Head of Product
1. VP of Product
1. VP of Engineering
1. CEO needs to merge the change

## DevOps Stages

![DevOps Loop](devops-loop-and-spans.png)

## Dev department

### Dev leadership

- Product: [Job van der Voort]
- Backend: [Tommy Morgan]
- Product Marketing: [Ashish Kuthiala]
- Content Marketing: [Erica Lindberg]
- Technical Writing: [Mike Lewis]

### Dev stages

**Key** \| PM: Product Manager \| PMM: Product Marketing Manager \| EM: Engineering Manager \| FEM: Frontend Engineering Manager \| CM: Content Marketing \| TW: Technical Writer

<%= partial("includes/product/categories", locals: { dev_ops: 'dev'}) %>
4. **Distribution** \| PM: [Joshua Lambert] \| PMM: [William Chia] \| EM: [Marin Jankovski] \| FEM: [Clement Ho] \| TW: [Axil]
    - [Omnibus](https://docs.gitlab.com/omnibus/)
    - [Cloud Native Installation](https://docs.gitlab.com/ee/install/kubernetes/) <kbd>New in 2018</kbd>
5. **Gitaly and Gitter** \| PM: [James Ramsay] \| PMM: [John Jeremiah] \| EM: [Tommy (interim)] \| FEM: [Tim Z (Interim)] \| TW: [Axil]
6. **Geo** \| PM: [Andreas Kämmerle] \| PMM: [John Jeremiah] \| EM: [Rachel Nienaber] \| FEM: [André Luís (Interim)] \| TW: [Evan Read]
7. [**Growth**](growth-team/)  \| PM: [Tamas Szuromi] \| EM: [Liam McAndrew] \| FEM: [Dennis Tang (Interim)]
8. **Sync** \| Currently part of Manage
    - [License Sync](https://gitlab.com/groups/gitlab-org/-/epics/456)
    - license.gitlab.com
    - version.gitlab.com
    - customers.gitlab.com (Subscription portal)
    - Usage data

## Ops department

### Ops leadership

- Product: [Kenny Johnston]
- Backend: [Dalia Havens]
- Product Marketing: [Ashish Kuthiala]
- Content Marketing: [Erica Lindberg]
- Technical Writing: [Mike Lewis]

### Ops stages

**Key** \| PM: Product Manager \| PMM: Product Marketing Manager \| EM: Engineering Manager \| FEM: Frontend Engineering Manager \| CM: Content Marketing \| TW: Technical Writer

<%= partial "includes/product/categories", locals: { dev_ops: 'ops'} %>

## Maturity

Not all categories are at the same level of maturity. Some are just minimal and
some are lovable. See the [category maturity page](maturity/) to see where each
category stands.

## Solutions

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)
1. Application Performance Monitoring (APM) = Metrics + Tracing + Real User Monitoring (RUM)

## Other functionality

This list of other functionality so you can easily find the team that owns it.
Maybe we should make our features easier to search to replace the section below.

### Other functionality in Manage

- User management, LDAP, signup, abuse, profiles
- Projects (Project creation, project templates, project import/export, importers)
- Groups and Subgroups
- GitLab.com specific functionality
- Navigation
- Admin Area

### Other functionality in Plan

- markdown functionality
- assignees
- milestones
- time tracking
- due dates
- labels
- issue weights
- quick actions
- email notifications
- todos
- search
- Elasticsearch integration
- Jira and other third-party issue management integration
- [gitlab-elasticsearch-indexer](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer)

### Other functionality in Create

- [gitlab-shell](https://gitlab.com/gitlab-org/gitlab-shell)
- [gitlab-workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse)

[Mark Pundsack]: /company/team/#MarkPundsack
[Job van der Voort]: /company/team/#Jobvo
[Ashish Kuthiala]: /company/team/#kuthiala
[Tommy Morgan]: /company/team/#tommy.morgan
[Dalia Havens]: /company/team/#dhavens
[Erica Lindberg]: /company/team/#EricaLindberg_
[Rachel Nienaber]: /company/team/#rnienaber
[André Luís (Interim)]: /company/team/#andr3
[Tommy (interim)]: /company/team/#tommy.morgan
[Jeremy Watson]: /company/team/#d3arWatson
[Fabio Busatto]: /company/team/#bikebilly
[Joshua Lambert]: /company/team/#joshlambert
[James Ramsay]: /company/team/#jamesramsay
[Victor Wu]: /company/team/#victorwuky
[Daniel Gruesso]: /company/team/#danielgruesso
[Andreas Kämmerle]: /company/team/#andreasmarc
[Jason Lenny]: /company/team/#j4lenn
[John Jeremiah]: /company/team/#j_jeremiah
[Liam McAndrew]: /company/team/#lmcandrew
[Tim Z (Interim)]: /company/team/#tpmtim
[Sean McGivern]: /company/team/#mcgivernsa
[André Luís]: /company/team/#andr3
[Douwe Maan]: /company/team/#DouweM
[William Chia]: /company/team/#thewilliamchia
[Marin Jankovski]: /company/team/#maxlazio
[Clement Ho]: /company/team/#ClemMakesApps
[Aricka]: /company/team/#arickaflowers
[Suri]: /company/team/#suripatel
[Pages]: /product/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /product/service-desk/
[Mike Lewis]: /company/team/#miketechnically
[Evan Read]: /company/team/#eread
[Axil]: /company/team/#_axil
[Kenny Johnston]: /company/team/#kencjohnston
[Tamas Szuromi]: /company/team/#metricbrew
[Dennis Tang (Interim)]: /company/team/#dennis
